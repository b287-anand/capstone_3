import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { useParams, Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import './Home.css';


export default function Home() {
	const navigate = useNavigate();

	const data = { 
		title: "VELOCITY RIDE",
		content: "UNLEASH THE ADVENTURE",
		destination: "/products",
		label: "Shop Now",

		  titleStyles: {
		 
    fontSize: "64px",      // Set the desired font size
    fontFamily: "Quicksand",   // Set the desired font family
    fontWeight: "bold",
     color: "white" ,
      // You can also adjust other styles like font weight
    // Add more styles as needed
  },
  contentStyles:{
  	fontSize: "24px", 
  	fontFamily: "Roboto", 
  	 color: "white" ,  // Set the desired font family
    

  },
 
  }
   const handle=()=>{
  	navigate('/products');
	};

	return(
		<>
	
			<div className=".navbar-text">
			<Banner data={data} labelOnClick={handle} />
			<Highlights />
			</div>


		</>
	)
};